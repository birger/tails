# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2018-11-07 16:30+0000\n"
"PO-Revision-Date: 2019-01-09 11:42+0000\n"
"Last-Translator: Chre <tor@renaudineau.org>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"start-new\">Restart on the new Tails</h1>\n"
msgstr "<h1 id=\"start-new\">Redémarrer sur le nouveau Tails</h1>\n"

#. type: Bullet: '1. '
msgid "Shut down the computer."
msgstr "Éteindre l'ordinateur."

#. type: Plain text
#, no-wrap
msgid "   <div class=\"step-image\">[[!img install/inc/infography/restart-on-new-tails.png link=\"no\" alt=\"USB stick unplugged on the left and computer restarted on USB stick on the right\"]]</div>\n"
msgstr "   <div class=\"step-image\">[[!img install/inc/infography/restart-on-new-tails.png link=\"no\" alt=\"Clé USB débranchée sur la gauche et ordinateur redémarrant avec la clé USB sur la droite\"]]</div>\n"

#. type: Bullet: '1. '
msgid "Unplug the other USB stick and leave the new USB stick plugged in."
msgstr "Débrancher l'autre clé USB et laisser la nouvelle clé USB branchée."

#. type: Bullet: '1. '
msgid "Switch on the computer."
msgstr "Allumer l'ordinateur."

#. type: Plain text
#, no-wrap
msgid "   [[!inline pages=\"install/inc/steps/mac_startup_disks.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "   [[!inline pages=\"install/inc/steps/mac_startup_disks.inline.fr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Bullet: '1. '
msgid ""
"The <span class=\"application\">Boot Loader Menu</span> appears and Tails "
"starts automatically after 4 seconds."
msgstr ""
"Le <span class=\"application\">menu du chargeur d’amorçage</span> apparaît "
"et Tails démarre automatiquement après 4 secondes."

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/screenshots/boot_loader_menu.png link=\"no\" alt=\"Black screen with Tails artwork. Boot Loader Menu with two options 'Tails' and 'Tails (Troubleshooting Mode)'.\"]]\n"
msgstr "   [[!img install/inc/screenshots/boot_loader_menu.png link=\"no\" alt=\"Écran noir avec le dessin de Tails. Menu du chargeur d’amorçage avec deux options 'Tails' et 'Tails (Troubleshooting Mode)'.\"]]\n"

#. type: Bullet: '1. '
msgid ""
"After 30&ndash;60 seconds, <span class=\"application\">Tails Greeter</span> "
"appears."
msgstr ""
"Après 30&ndash;60 secondes, le <span class=\"application\">Tails Greeter</"
"span> apparaît."

#. type: Plain text
#, no-wrap
msgid ""
"1. In <span class=\"application\">Tails Greeter</span>, select your language and\n"
"keyboard layout in the <span class=\"guilabel\">Language & Region</span> section.\n"
"Click <span class=\"button\">Start Tails</span>.\n"
msgstr ""
"1. Dans le <span class=\"application\">Tails Greeter</span>, sélectionnez votre langue et\n"
"votre disposition de clavier dans la section <span class=\"guilabel\">Langue et région</span>.\n"
"Cliquez sur <span class=\"button\">Démarrer Tails</span>.\n"

#. type: Bullet: '1. '
msgid "After 15&ndash;30 seconds, the Tails desktop appears."
msgstr "Après 15&ndash;30 secondes, le bureau de Tails apparaît."
