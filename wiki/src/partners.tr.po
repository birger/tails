# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-09-18 15:06+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: outside any tag (error?)
msgid "[[!meta title=\"Partners\"]]"
msgstr ""

#. type: Content of: <p>
msgid ""
"These organizations, companies and individuals provide financial support to "
"Tails through grants or donations. Thanks to their substantial support we "
"are able to maintain and improve Tails. Thank you!"
msgstr ""

#. type: Content of: <h1>
msgid "Becoming a partner"
msgstr ""

#. type: Content of: <p>
msgid ""
"Are you a company or organization and want to help Tails? [[Become a "
"partner!|partners/become/]]"
msgstr ""

#. type: Content of: <p>
msgid ""
"Are you an individual and want to help Tails? [[We warmly welcome your "
"donations!|donate]]"
msgstr ""

#. type: Content of: <h1>
msgid "Current partners"
msgstr ""

#. type: Content of: <p>
msgid "&gt; $100&#8239;000"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Handshake Foundation - $200 000"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"https://handshake.org/\" target=\"_blank\">[[!img handshake.png "
"link=\"no\" alt=\"Handshake Foundation logo\"]]</a>"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Mozilla Open Source Support - $146 530"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"https://mozilla.org/\" target=\"_blank\">[[!img mozilla.png link="
"\"no\"]]</a>"
msgstr ""

#. type: Content of: <p>
msgid "$10&#8239;000 – $50&#8239;000"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "ISC - 20 743€"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img isc.png link=\"no\" alt=\"Counterpart International logo\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 4.5 btc"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img anonymous.png link=\"no\"]]"
msgstr ""

#. type: Content of: <p>
msgid "$1&#8239;000 – $9&#8239;999"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "An individual - 2 000€"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 0.2 btc"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 0.196 btc"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 0.19 btc"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "A group of people - 1 000€"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "An individual - 1 000€"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "An individual - $1 000"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "TOP10VPN - $1 000"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"https://www.top10vpn.com/\" target=\"_blank\">[[!img top10vpn.png "
"link=\"no\" alt=\"TOP10VPN logo\"]]</a>"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "DeepOnion - 0.154 btc"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"https://deeponion.org/\" target=\"_blank\">[[!img deeponion.png "
"link=\"no\" alt=\"DeepOnion logo\"]]</a>"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Cooltechzone - $1 000"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"https://cooltechzone.com/\" target=\"_blank\">[[!img cooltechzone."
"png link=\"no\" alt=\"Best VPN zone 2019 logo\"]]</a>"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "VPN Review - $1 000"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"https://vpn-review.com/\" target=\"_blank\">[[!img vpnreview.png "
"link=\"no\" alt=\"VPN review logo\"]]</a>"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "The Best VPN - $1 000"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"https://thebestvpn.com/\" target=\"_blank\">[[!img bestvpn.png "
"link=\"no\" alt=\"The Best VPN logo\"]]</a>"
msgstr ""

#. type: Content of: <p>
msgid "in-kind"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Tor - Server rent"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"https://torproject.org\" target=\"_blank\">[[!img tor.png link=\"no"
"\" alt=\"Tor logo\"]]</a>"
msgstr ""

#. type: Content of: <h1>
msgid "Previous partners"
msgstr ""

#. type: Content of: outside any tag (error?)
msgid "<span class=\"clearfix\"></span>"
msgstr ""

#. type: Content of: <h2>
msgid "2018"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Open Technology Fund - $132 390"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img otf.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "DuckDuckGo donation challenge - $38 080"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img ddg.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Lush Digital Fund - 11 000€"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img lush.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Freedom of the Press Foundation - $3 954"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img fpf.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "An individual - $2 000"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "I2P - 0.1 btc"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img i2p.png link=\"no\"]]"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img tor.png link=\"no\"]]"
msgstr ""

#. type: Content of: <h2>
msgid "2017"
msgstr ""

#. type: Content of: <p>
msgid "$50&#8239;000 – $99&#8239;000"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Mozilla Open Source Support - $77 000"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img mozilla.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "ExpressVPN - $1 000"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img expressvpn.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 0.5 btc"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 5 btc"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 0.314 btc"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 0.347 btc"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 2 btc"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 0.062 btc"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 0.1 btc"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Tor - Travel to Tor meeting & server rent"
msgstr ""

#. type: Content of: <h2>
msgid "2016"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Open Technology Fund - $208 800"
msgstr ""

#. type: Content of: <p>
msgid "$10&#8239;000 – $49&#8239;999"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 11 609€"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "An individual - $5 000"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 2 902€"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Mediapart - 2 000€"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img mediapart.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 2 554€"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 1 857€"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 1 433€"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Anonymous donation - 1 161€"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Google - GSoC Tails Server"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img gsoc.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Tor - Travel sponsorship & server rent"
msgstr ""

#. type: Content of: <h2>
msgid "2015"
msgstr ""

#. type: Content of: <p>
msgid "$50&#8239;000 – $99&#8239;999"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Hivos International - 70 000€"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img hivos.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "DuckDuckGo - $25 000"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Laura Poitras & Edward Snowden, Ridenhour's film award - $10 000"
msgstr ""

#. type: Content of: <div><p>
msgid "Laura Poitras &"
msgstr ""

#. type: Content of: <div><p>
msgid "Edward Snowden"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Freedom of the Press Foundation - $8 859"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Localization Lab - Translation to Farsi"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img localizationlab.png link=\"no\"]]"
msgstr ""

#. type: Content of: <h2>
msgid "2014"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Access Now - 50 000€"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img accessnow.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Freedom of the Press Foundation - $33 377"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Open Internet Tools - $25 800"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img openitp.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Förderung Freier Information und Software - 5 000€"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img ffis.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Debian - $5 000"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img debian.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Tor - $5 000"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Mozilla - Travel to Tails hackfest"
msgstr ""

#. type: Content of: <h2>
msgid "2013"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "National Democratic Institute - $21 000"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img ndi.png link=\"no\"]]"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Tor - $20 000"
msgstr ""

#. type: Content of: <h2>
msgid "2012"
msgstr ""

#. type: Content of: <h2>
msgid "2011"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Tor - $10 950"
msgstr ""

#. type: Content of: <h2>
msgid "2010"
msgstr ""

#. type: Attribute 'title' of: <div><p>
msgid "Tor - 8 500€"
msgstr ""
