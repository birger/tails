# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-09-27 21:13+0000\n"
"PO-Revision-Date: 2019-10-24 10:30+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: \n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.20\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Contact\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"There are many ways to contact us, depending on what you want to talk about."
msgstr ""

#. type: Plain text
msgid "All mailing lists are in English unless specified otherwise."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Public mailing lists\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>These mailing lists are public: subscription is open to anyone. Don't\n"
"send compromising information. Please respect the\n"
"[[code of conduct|contribute/working_together/code_of_conduct/]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"amnesia-news\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "amnesia-news\n"
msgstr ""

#. type: Plain text
msgid ""
"amnesia-news@boum.org is the mailing list where we send our [[news]] by "
"email. It has a low traffic and it is the right place to stay up-to-date "
"with the releases and security announcements."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>\n"
"<form method=\"POST\" action=\"https://www.autistici.org/mailman/subscribe/amnesia-news\">\n"
"\t<input class=\"text\" name=\"email\" value=\"\"/>\n"
"\t<input class=\"button\" type=\"submit\" value=\"Subscribe\"/>\n"
"</form>\n"
"</p>\n"
msgstr ""

#. type: Plain text
msgid ""
"Public archive of amnesia-news: <https://lists.autistici.org/list/amnesia-"
"news.html>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>\n"
"<form action=\"https://lists.autistici.org/cgi-lurker/keyword.cgi\" accept-charset=\"UTF-8\">\n"
"    <input type=\"hidden\" name=\"doc-url\" value=\"https://lists.autistici.org\">\n"
"\t<input type=\"hidden\" name=\"format\" value=\"en.html\">\n"
"\t<input type=\"text\"   name=\"query\" value=\"\" class=\"longtext\" placeholder=\"ml:amnesia-news search terms\" onclick=\"value='ml:amnesia-news '\">\n"
"\t<input type=\"submit\" name=\"submit\" value=\"Search archive\">\n"
"</form>\n"
"</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-project\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "tails-project\n"
msgstr ""

#. type: Plain text
msgid ""
"tails-project@boum.org is the mailing list where we discuss upcoming events, "
"monthly reports, and other non-technical matters."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>\n"
"<form method=\"POST\" action=\"https://www.autistici.org/mailman/subscribe/tails-project\">\n"
"\t<input class=\"text\" name=\"email\" value=\"\"/>\n"
"\t<input class=\"button\" type=\"submit\" value=\"Subscribe\"/>\n"
"</form>\n"
"</p>\n"
msgstr ""

#. type: Plain text
msgid ""
"Public archive of tails-project: <https://lists.autistici.org/list/tails-"
"project.html>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>\n"
"<form action=\"https://lists.autistici.org/cgi-lurker/keyword.cgi\" accept-charset=\"UTF-8\">\n"
"    <input type=\"hidden\" name=\"doc-url\" value=\"https://lists.autistici.org\">\n"
"\t<input type=\"hidden\" name=\"format\" value=\"en.html\">\n"
"\t<input type=\"text\"   name=\"query\" value=\"\" class=\"longtext\" placeholder=\"ml:tails-project search terms\" onclick=\"value='ml:tails-project '\">\n"
"\t<input type=\"submit\" name=\"submit\" value=\"Search archive\">\n"
"</form>\n"
"</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-dev\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "tails-dev\n"
msgstr ""

#. type: Plain text
msgid ""
"tails-dev@boum.org is the mailing list where the development work is "
"coordinated and technical design questions are discussed. Subscribe if you "
"want to [[contribute code|contribute/how/code]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>\n"
"<form method=\"POST\" action=\"https://www.autistici.org/mailman/subscribe/tails-dev\">\n"
"\t<input class=\"text\" name=\"email\" value=\"\"/>\n"
"\t<input class=\"button\" type=\"submit\" value=\"Subscribe\"/>\n"
"</form>\n"
"</p>\n"
msgstr ""

#. type: Plain text
msgid ""
"Public archive of tails-dev: <https://lists.autistici.org/list/tails-dev."
"html>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>\n"
"<form action=\"https://lists.autistici.org/cgi-lurker/keyword.cgi\" accept-charset=\"UTF-8\">\n"
"    <input type=\"hidden\" name=\"doc-url\" value=\"https://lists.autistici.org\">\n"
"\t<input type=\"hidden\" name=\"format\" value=\"en.html\">\n"
"\t<input type=\"text\"   name=\"query\" value=\"\" class=\"longtext\" placeholder=\"ml:tails-dev search terms\" onclick=\"value='ml:tails-dev '\">\n"
"\t<input type=\"submit\" name=\"submit\" value=\"Search archive\">\n"
"</form>\n"
"</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-testers\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "tails-testers\n"
msgstr ""

#. type: Plain text
msgid ""
"tails-testers@boum.org is the mailing list for people who want to help "
"[[test|contribute/how/testing]] new releases or features."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>\n"
"<form method=\"POST\" action=\"https://www.autistici.org/mailman/subscribe/tails-testers\">\n"
"\t<input class=\"text\" name=\"email\" value=\"\"/>\n"
"\t<input class=\"button\" type=\"submit\" value=\"Subscribe\"/>\n"
"</form>\n"
"</p>\n"
msgstr ""

#. type: Plain text
msgid ""
"Public archive of tails-testers: <https://lists.autistici.org/list/tails-"
"testers.html>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>\n"
"<form action=\"https://lists.autistici.org/cgi-lurker/keyword.cgi\" accept-charset=\"UTF-8\">\n"
"    <input type=\"hidden\" name=\"doc-url\" value=\"https://lists.autistici.org\">\n"
"\t<input type=\"hidden\" name=\"format\" value=\"en.html\">\n"
"\t<input type=\"text\"   name=\"query\" value=\"\" class=\"longtext\" placeholder=\"ml:tails-testers search terms\" onclick=\"value='ml:tails-testers '\">\n"
"\t<input type=\"submit\" name=\"submit\" value=\"Search archive\">\n"
"</form>\n"
"</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-ux\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "tails-ux\n"
msgstr ""

#. type: Plain text
msgid ""
"tails-ux@boum.org is the list for matters related to [[user experience and "
"user interface|contribute/how/user_experience]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>\n"
"<form method=\"POST\" action=\"https://www.autistici.org/mailman/subscribe/tails-ux\">\n"
"\t<input class=\"text\" name=\"email\" value=\"\"/>\n"
"\t<input class=\"button\" type=\"submit\" value=\"Subscribe\"/>\n"
"</form>\n"
"</p>\n"
msgstr ""

#. type: Plain text
msgid ""
"Public archive of tails-ux: <https://lists.autistici.org/list/tails-ux.html>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>\n"
"<form action=\"https://lists.autistici.org/cgi-lurker/keyword.cgi\" accept-charset=\"UTF-8\">\n"
"    <input type=\"hidden\" name=\"doc-url\" value=\"https://lists.autistici.org\">\n"
"\t<input type=\"hidden\" name=\"format\" value=\"en.html\">\n"
"\t<input type=\"text\"   name=\"query\" value=\"\" class=\"longtext\" placeholder=\"ml:tails-ux search terms\" onclick=\"value='ml:tails-ux '\">\n"
"\t<input type=\"submit\" name=\"submit\" value=\"Search archive\">\n"
"</form>\n"
"</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-l10n\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "tails-l10n\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"contribute/how/translate/tails-l10n.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Private email addresses\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-support-private\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "tails-support-private\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"You can write encrypted emails to <tails-support-private@boum.org> if\n"
"you need help in private.\n"
msgstr ""

#. type: Plain text
msgid ""
"[[OpenPGP key|tails-bugs.key]] ([[details|doc/about/openpgp_keys#support]])."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"support/talk/languages.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-press\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "tails-press\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"If you are a journalist and want to write about Tails, or if you want to\n"
"send us links to [[press articles|press]] about Tails, write to\n"
"<tails-press@boum.org>.\n"
msgstr ""

#. type: Plain text
msgid ""
"[[OpenPGP key|tails-press.key]] ([[details|doc/about/openpgp_keys#press]])."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-accounting\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"tails-accounting\n"
"-----------------\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "If you want to fund Tails, write to <tails-accounting@boum.org>.\n"
msgstr ""

#. type: Plain text
msgid ""
"[[OpenPGP key|tails-accounting.key]] ([[details|doc/about/"
"openpgp_keys#accounting]])."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-foundations\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "tails-foundations\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"To get in touch with the Tails [[Foundations\n"
"Team|contribute/working_together/roles/foundations_team]], write to\n"
"<tails-foundations@boum.org>.\n"
msgstr ""

#. type: Plain text
msgid ""
"[[OpenPGP key|tails-foundations.key]] ([[details|doc/about/"
"openpgp_keys#foundations]])."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-mirrors\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"tails-mirrors\n"
"-----------------\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "If you want to operate a Tails mirror, write to <tails-mirrors@boum.org>.\n"
msgstr ""

#. type: Plain text
msgid ""
"[[OpenPGP key|tails-mirrors.key]] ([[details|doc/about/"
"openpgp_keys#mirrors]])."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-sysadmins\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "tails-sysadmins\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"To talk about our infrastructure (servers, test suite, repositories,\n"
"mirrors, etc.), write to <tails-sysadmins@boum.org>.\n"
msgstr ""

#. type: Plain text
msgid ""
"[[OpenPGP key|tails-sysadmins.key]] ([[details|doc/about/"
"openpgp_keys#sysadmins]])."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails-translations\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "tails-translations\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "To talk about the [[Tails translation platform|contribute/how/translate/with_translation_platform]], write to <tails-translations@boum.org>.\n"
msgstr ""

#. type: Plain text
msgid ""
"[[OpenPGP key|tails-translations.key]] ([[details|doc/about/"
"openpgp_keys#translations]])."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tails\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "tails\n"
msgstr ""

#. type: Plain text
msgid ""
"For matters that are listed in none of the above and for vulnerabilities "
"disclosures, you can write encrypted emails to <tails@boum.org>."
msgstr ""

#. type: Plain text
msgid ""
"[[OpenPGP key|tails-email.key]] ([[details|doc/about/openpgp_keys#private]])."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"chat\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Chat rooms\n"
msgstr ""

#. type: Plain text
msgid ""
"You can join our [[users chat room|support/chat]] and [[developers chat room|"
"contribute/chat]]. Only a few Tails developers hang out there, so email is "
"preferred for anything that might be of interest for the larger community."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"legal\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Legal representative\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<pre>\n"
"Center for the Cultivation of Technology gemeinnuetzige GmbH\n"
"Gottschedstrasse 4\n"
"13357 Berlin\n"
"Germany\n"
"Tel. +49 30 120835 9601\n"
"Handelsregister Berlin Charlottenburg, HRB 180673 B. CEO: Moritz Bartl.\n"
"</pre>\n"
msgstr ""
