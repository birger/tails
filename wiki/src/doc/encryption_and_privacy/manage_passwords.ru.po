# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-08-29 15:15+0200\n"
"PO-Revision-Date: 2018-07-02 10:46+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Manage passwords with KeePassXC\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Using the [<span class=\"application\">KeePassXC</span>](https://keepassxc.org/)\n"
"password manager you can:\n"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Store many passwords in an encrypted database which is protected by a single "
"passphrase of your choice."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Always use different and stronger passwords, since you only have to remember "
"a single passphrase to unlock the entire database."
msgstr ""

#. type: Bullet: '  - '
msgid "Generate very strong random passwords."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"create\"></a>\n"
msgstr "<a id=\"create\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Create and save a password database\n"
msgstr ""

#. type: Plain text
msgid ""
"Follow these steps to create a new password database and save it in the "
"persistent volume for use in future working sessions."
msgstr ""

#. type: Plain text
msgid ""
"To learn how to create and configure the persistent volume, read the "
"[[documentation on persistence|first_steps/persistence]]."
msgstr ""

#. type: Bullet: '0. '
msgid ""
"When starting Tails, [[enable the persistent volume|first_steps/persistence/"
"use]]."
msgstr ""

#. type: Bullet: '0. '
msgid ""
"In the [[<span class=\"application\">Persistent Volume Assistant</span>|"
"first_steps/persistence/configure]], verify that the [[<span class=\"guilabel"
"\">Personal Data</span> persistence feature|doc/first_steps/persistence/"
"configure#personal_data]] is activated. If it is deactivated, activate it, "
"restart Tails, and [[enable the persistent volume|first_steps/persistence/"
"use]]."
msgstr ""

#. type: Bullet: '0. '
msgid ""
"To start <span class=\"application\">KeePassXC</span>, choose <span class="
"\"menuchoice\"> <span class=\"guimenu\">Applications</span>&nbsp;▸ <span "
"class=\"guisubmenu\">Accessories</span>&nbsp;▸ <span class=\"guimenuitem"
"\">KeePassXC</span></span>."
msgstr ""

#. type: Bullet: '0. '
msgid ""
"To create a new database, click <span class=\"guilabel\">Create new "
"database</span>."
msgstr ""

#. type: Bullet: '   * '
msgid "Save the database as *keepassx.kdbx* in the *Persistent* folder."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"0. The database is encrypted and protected by\n"
"   a passphrase.\n"
"   * Specify a passphrase of your choice in the <span\n"
"     class=\"guilabel\">Enter password</span> text box.\n"
"   * Type the same passphrase again in the <span class=\"guilabel\">Repeat\n"
"     password</span> text box.\n"
"   * Click <span class=\"guilabel\">OK</span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"restore\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Restore and unlock the password database\n"
msgstr ""

#. type: Plain text
msgid ""
"Follow these steps to unlock the password database saved in the persistent "
"volume from a previous working session."
msgstr ""

#. type: Bullet: '0. '
msgid ""
"If you have a database named *keepassx.kdbx* in your *Persistent* folder, "
"<span class=\"application\">KeePassXC</span> automatically displays a dialog "
"to unlock that database."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   Enter the passphrase for this database and click <span class=\"guilabel\">OK</span>.\n"
msgstr ""

#. type: Bullet: '0. '
msgid "If you enter an invalid passphrase the following error message appears:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <span class=\"guilabel\">Unable to open the database.<br/>\n"
"   Wrong key or database file is corrupt.</span>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>In addition to the <em>password database</em>, you can store your <span class=\"application\">KeePassXC</span>\n"
"<em>settings</em>\n"
"using the [[<span class=\"guilabel\">Dotfiles</span> persistence\n"
"feature|doc/first_steps/persistence/configure#dotfiles]]. To do so, create the folder\n"
"<code>/live/persistence/TailsData_unlocked/dotfiles/.config/keepassxc/</code>\n"
"and copy the file <code>~/.config/keepassxc/keepassxc.ini</code> to it.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid "<a id=\"kbdx4\"></a>\n"
msgstr "<a id=\"create\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Update the cryptographic parameters of your password database\n"
msgstr ""

#. type: Plain text
msgid ""
"KeePassXC, included in Tails 4.0 and later, supports the [KBDX 4 file format]"
"(https://keepass.info/help/kb/kdbx_4.html). The KBDX 4 file format uses "
"stronger cryptographic parameters than previous file formats. The parameters "
"of previous file formats are still secure."
msgstr ""

#. type: Plain text
msgid "To update your database to the latest cryptographic parameters:"
msgstr ""

#. type: Bullet: '0. '
msgid ""
"Choose <span class=\"menuchoice\"> <span class=\"guimenu\">Database</"
"span>&nbsp;▸ <span class=\"guimenuitem\">Database settings</span></span>."
msgstr ""

#. type: Bullet: '0. '
msgid ""
"In the <span class=\"guilabel\">Encryption</span> tab, change the following "
"parameters:"
msgstr ""

#. type: Bullet: '   * '
msgid "Set <span class=\"guilabel\">Encryption Algorithm</span> to *ChaCha20*."
msgstr ""

#. type: Bullet: '   * '
msgid ""
"Set <span class=\"guilabel\">Key Derivation Function</span> to *Argon2*."
msgstr ""

#. type: Bullet: '0. '
msgid "Click <span class=\"button\">OK</span>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"migration\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Migrating a password database from Tails 2.12 and earlier\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"The database format of <span class=\"application\">KeePass</span> 1\n"
"(Tails 2.12 and earlier) is incompatible with the database format of\n"
"<span class=\"application\">KeePassXC</span> (Tails 4.0 and later).\n"
msgstr ""

#. type: Plain text
msgid "To migrate your database to the new format:"
msgstr ""

#. type: Bullet: '0. '
msgid "Start <span class=\"application\">KeePassXC</span>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"0. Choose <span class=\"menuchoice\">\n"
"     <span class=\"guimenu\">Database</span>&nbsp;▸\n"
"     <span class=\"guimenu\">Import</span>&nbsp;▸\n"
"     <span class=\"guimenuitem\">Import KeePass 1 database</span></span>.\n"
msgstr ""

#. type: Bullet: '0. '
msgid ""
"Select your database, for example <span class=\"filename\">keepassx.kdb</"
"span>."
msgstr ""

#. type: Bullet: '0. '
msgid "After your database is open, save it to the new format:"
msgstr ""

#. type: Bullet: '   * '
msgid ""
"Choose <span class=\"menuchoice\"> <span class=\"guimenu\">Database</"
"span>&nbsp;▸ <span class=\"guimenuitem\">Save database</span></span>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   Note that only the file extension is different:\n"
msgstr ""

#. type: Bullet: '   * '
msgid "*kdb* for the old format."
msgstr ""

#. type: Bullet: '   * '
msgid "*kdbx* for the new format."
msgstr ""

#. type: Bullet: '0. '
msgid ""
"This operation does not delete your old database from your *Persistent* "
"folder."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   You can now delete your old database or keep it as a backup.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"user_guide\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Additional documentation\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"For more detailed instructions on how to use\n"
"<span class=\"application\">KeePassXC</span>, refer to the\n"
"[<span class=\"application\">KeePassXC</span> guide\n"
"of the Electronic Frontier Foundation](https://ssd.eff.org/en/module/how-use-keepassxc).\n"
msgstr ""
